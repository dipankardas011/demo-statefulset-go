FROM golang:1.20 as builder

WORKDIR /app

COPY . .

RUN go get -d -v

RUN CGO_ENABLED=0 go build -o demo

FROM scratch

COPY --from=builder /app/demo /app/demo

ENTRYPOINT [ "/app/demo" ]

